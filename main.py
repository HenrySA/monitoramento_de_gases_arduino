import paho.mqtt.client as mqtt
import apiPI

# Define MQTT broker information
broker_address = "34.234.193.23"
broker_port = 1883

# Define callback function for when a message is received
def on_message(client, userdata, message):
    
    # print("Topic: " + message.topic)
    # print("Payload: " + str(message.payload))

    if ( message.topic == '/lux/teste' ):

        lux                 = message.payload.decode()
        ptM_lux             = apiPI.getPiPoints('monitoramento_dgas_lux')[0]

        print(apiPI.setValue(ptM_lux['WebId'], lux))


    elif( message.topic == '/pressao/teste' ):

        pres                 = message.payload.decode()
        ptM_pres             = apiPI.getPiPoints('monitoramento_dgas_pres')[0]

        print(apiPI.setValue(ptM_pres['WebId'], pres))

    elif( message.topic == '/temperatura/teste' ):

        temp                 = message.payload.decode()
        ptM_temp             = apiPI.getPiPoints('monitoramento_dgas_temp')[0]

        print(apiPI.setValue(ptM_temp['WebId'], temp))

    elif( message.topic == '/umidade/teste' ):

        umi                 = message.payload.decode()
        ptM_umi             = apiPI.getPiPoints('monitoramento_dgas_umi')[0]

        print(apiPI.setValue(ptM_umi['WebId'], umi))

    elif( message.topic == '/voc/teste' ):

        voc                 = message.payload.decode()
        ptM_voc             = apiPI.getPiPoints('monitoramento_dgas_voc')[0]

        print(apiPI.setValue(ptM_voc['WebId'], voc))

    else:
        print("Erro ao enviar")


# Create MQTT client instance and connect to broker
client = mqtt.Client()
client.connect(broker_address, broker_port)

# Subscribe to 5 different topics
topics = ["/lux/teste", "/umidade/teste", "/temperatura/teste", "/voc/teste", "/pressao/teste"]
for topic in topics:
    client.subscribe(topic)

# Set callback function for message received event
client.on_message = on_message

# Loop indefinitely to receive messages
client.loop_forever()
